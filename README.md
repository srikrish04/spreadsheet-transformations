Overview
--------

This program reads tab separated data(with multiple lines) from a text file and converts it into Java objects for manipulation.
Manipulations such as reading from a file, writing to a file, adding new column, calculate specific equation, print all records
and print top 5 rows are performed in this program.

The solution uses below informations from the input file.

-File consist of tab separated datas.
-First line of each file usually specifies ColumnName, subsequent lines can be String value or Integer.

To add new data to the File and perform calculations.

-create a integer rowCnt, as global variable. increment this variable when operations is performed on each lines of the inputFile.
-create a List that holds Map of <Integer,String> , name it as tableColumnData.
-Map<Integer,String> stores index of each column data as Integer and its column value as String.


File and external data
----------------------


There are two files in this program.
	-DataTransformer.java - program to calculate a equation, read from a file, write to file, add new column and print data.
	-DataChangeUI.java - main program to invoke methods from DataTransformer.java class
	
spreadSheetCalculator-- is a included method to add modularity to the code , which excludes reduntant tasks of adding,
						substracting, multiple and divide on same set of operations.
						


Data structures and their relations to each other
-------------------------------------------------

The program uses ArrayList<HashMap<Integer,String>> where List store each line/row from the file.and Map 
stores Key value pair of each index and its values of each column data. 
This program uses map.entrySet() to get Key of the Map from a Value.

tableColumnData-- Stores List of rows as HashMap.
HashMap-- store index of each element in the row and related column value.
 

Assumptions
-----------


- No two por more columns can have same column Name.
-Adding new column to a file , creates ColumnName give in the first line with 0 as values to its column associated.
-After clear operation is invoked, all datas in the Objects will be cleared.
-If add new column is created after clear is invoked, a new object is initialized and new columnName is added as
 first line and , next data line is set to value 0.
-new column name which matches already existing column name are checked case insensitively and cannot add column with same name.


Key algorithms and design elements
----------------------------------

The program one line of datas at a time.

read-- This method is handled to take empty file, null file as input and return no exception.
new Column-- This method is handled to gracefully terminate if invoked with empty or null input.
print-- checks if List is empty or null, if not prints the data from list, else shows error message.
top-- checks if List is empty or null, also check if size of list is greater than 5, if yes then prints first
	5 lines. else , print all lines.

write-- checks for empty list, return rowcount as zero .
		if list is not empty, writes all elements from the file.
		
calculate-- checks for equation is null	
			checks for equation empty
			checks for List empty or size is 0
			checks for List size is 1, it has no value to calculate.
			
			1)The given equation is splitted as string based on "=",
				which returns outputColumnName and actual equation to be performed.
			
			2)The actual equation is then split based on its operator(i.e., +,-,*,/),
				which returns leftoperand and rightoperand.
			3)getKey()- method is used to check these columnNames(output,inputs-Left operand & right operand).
				This method uses Map.entrySet() to get Key using values.
				
			4)Checks for operator and performs related calculation tasks.
			5)spreadSheetCalculator()-- method checks for if equation has integer value,
			columnNames in any combination and perform calculation.
			6)loads this output value to the specified output index.

 
Limitation
----------
-more than one operator in an equation will not be handled.
-calculate operators can only be performed when values are in integer.No other combinations is supported.

