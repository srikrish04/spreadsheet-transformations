import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataTransformer {
	ArrayList<HashMap<Integer, String>> tableColumnData = new ArrayList<HashMap<Integer, String>>();

	int rowCnt;

	public boolean clear() {
		// clears all Data from the object.
		tableColumnData = new ArrayList<HashMap<Integer, String>>();
		rowCnt = 0;
		if (tableColumnData.isEmpty()) {
			return true;
		}
		return false;
	}

	public Integer read(String filename) {
		if (filename != null) {
			File file = new File(filename);

			try {
				if (file.exists()) {
					BufferedReader buffRead = new BufferedReader(new FileReader(file));
					tableColumnData = new ArrayList<HashMap<Integer, String>>();
					String lineTxt;
					rowCnt = 0;
					// reads each line from the file
					while ((lineTxt = buffRead.readLine()) != null) {

						HashMap<Integer, String> columnVal = new HashMap<Integer, String>();

						String[] values = lineTxt.split("\t");

						for (int i = 0; i < values.length; i++) {
							columnVal.put(i, values[i]);
						}
						tableColumnData.add(columnVal);
						rowCnt++;

					}
					// checks if no rows in file
					if (rowCnt == 0) {
						System.out.println("No data in the specified file");
					}

					buffRead.close();
				} else {
					rowCnt = 0;
					System.out.println("File not found at the specified location");
				}
			} catch (FileNotFoundException e) {
				rowCnt = 0;
				System.out.println("File not found at the specified location");
			} catch (IOException e) {
				rowCnt = 0;
				System.out.println("Error occured. File unsupported.");
			}
		} else {
			System.out.println("Filename is null or invalid");
			return 0;
		}
		return rowCnt;
	}

	public boolean newColumn(String columnName) {
		boolean status = false;
		// checks for empty object, if empty instantiate the data objects and add new
		// column with one value as 0.
		try {

			if (!columnName.isEmpty()) {

				if (!tableColumnData.isEmpty()) {

					for (HashMap<Integer, String> row : tableColumnData) {
						//checks if columnName already exists and returns false
						for (int i = 0; i < row.size(); i++) {
							if (row.get(i).equalsIgnoreCase(columnName)) {
								System.out.println("Column Name already exist");
								return false;
							}
						}
					}

					tableColumnData.get(0).put(tableColumnData.get(0).size(), columnName);

					for (int i = 1; i < rowCnt; i++) {
						tableColumnData.get(i).put(tableColumnData.get(i).size(), "0");
					}
					status = true;
				} else {
					tableColumnData = new ArrayList<HashMap<Integer, String>>();
					rowCnt = 0;
					HashMap<Integer, String> rowDataVal = new HashMap<Integer, String>();
					rowDataVal.put(0, columnName);
					HashMap<Integer, String> rowDataValNewRow = new HashMap<Integer, String>();
					rowDataValNewRow.put(0, "0");
					tableColumnData.add(rowDataVal);
					tableColumnData.add(rowDataValNewRow);
					rowCnt = tableColumnData.size();
					status = true;
				}
			} else {
				System.out.println("Column Name cannot be empty");
			}
		} catch (NullPointerException e) {
			System.out.println("Column Name cannot be null");
		}
		return status;
	}

	public Integer calculate(String equation) {
		if (equation == null) {
			System.out.println("cannot calculate for null equation");
			return 0;
		}
		if (equation.isEmpty()) {
			System.out.println("cannot calculate for empty equation");
			return 0;
		}
		if (tableColumnData.isEmpty()) {
			System.out.println("No data to calculate");
			return 0;
		}
		if (tableColumnData.size() == 1) {
			System.out.println("No value for given column");
			return 0;
		}
		equation = equation.trim();
		String[] equaSplit = equation.split(" = ");
		String outputColumnName = equaSplit[0].trim();
		String inputsColumnOperands = equaSplit[1];
		Integer outputVal = 0;
		int keyIndexA = -1, keyIndexB = -1, outputKeyIndex = -1;

		// iterates only first value of the object and returns the key of columnName
		// from Map.
		for (HashMap<Integer, String> val : tableColumnData) {
			try {
				outputKeyIndex = getKey(val, outputColumnName);
			} catch (NullPointerException e) {
				System.out.println("columnName not found : " + outputColumnName);
				return 0;

			}
			break;
		}

		if (inputsColumnOperands.contains(" + ") || inputsColumnOperands.contains(" - ")
				|| inputsColumnOperands.contains(" * ") || inputsColumnOperands.contains(" / ")) {
			String[] inputs = inputsColumnOperands.split("\\s+");
			outputVal = spreadSheetCalculator(inputs, outputKeyIndex, keyIndexA, keyIndexB);
		} else {
			if (inputsColumnOperands.matches("-?\\d+")) {
				outputVal = 0;
				for (int i = 1; i < tableColumnData.size(); i++) {

					Double output = Double.parseDouble(inputsColumnOperands);
					tableColumnData.get(i).put(outputKeyIndex, String.valueOf(Math.round(output)));

					outputVal++;
				}

			} else if (inputsColumnOperands.matches("^[a-zA-Z]+$")) {
				outputVal = 0;
				for (HashMap<Integer, String> val : tableColumnData) {
					try {
						keyIndexA = getKey(val, inputsColumnOperands);
					} catch (NullPointerException e) {
						System.out.println("ColumnName not found :" + inputsColumnOperands);
						return 0;
					}
					break;
				}

				for (int i = 1; i < tableColumnData.size(); i++) {

					Double output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA));
					tableColumnData.get(i).put(outputKeyIndex, String.valueOf(Math.round(output)));
					outputVal++;

				}
			}
		}

		return outputVal;
	}

	public void top() {
		// checks for empty dataset
		if (!tableColumnData.isEmpty()) {
			// checks for rowCount greater than 5
			if (rowCnt > 5) {
				// if true prints first 5 rows
				for (int i = 0; i < 5; i++) {
					for (int j = 0; j < tableColumnData.get(i).size(); j++) {

						System.out.print(tableColumnData.get(i).get(j) + "\t");
					}

					System.out.println();
				}
			} else {
				// else prints all rows
				for (int i = 0; i < rowCnt; i++) {
					for (int j = 0; j < tableColumnData.get(i).size(); j++) {

						System.out.print(tableColumnData.get(i).get(j) + "\t");
					}

					System.out.println();
				}
			}
		} else {
			System.out.println("No Records to Print Top 5");
		}

	}

	public void print() {
		// checks for empty dataset
		if (!tableColumnData.isEmpty()) {
			for (int i = 0; i < rowCnt; i++) {
				for (int j = 0; j < tableColumnData.get(i).size(); j++) {
					System.out.print(tableColumnData.get(i).get(j) + "\t");
				}
				System.out.println("\n");
			}
		} else {
			System.out.println("No Records to Print All");
		}
	}

	public Integer write(String filename) {
		int writeCount = 0;
		// opens buffered writer to write into a file
		try (BufferedWriter buffWriter = new BufferedWriter(new FileWriter(filename))) {

			if (!tableColumnData.isEmpty()) {
				for (int i = 0; i < rowCnt; i++) {
					for (int j = 0; j < tableColumnData.get(i).size(); j++) {
						buffWriter.write(tableColumnData.get(i).get(j) + "\t");
					}
					buffWriter.write("\n");
					writeCount++;
				}

			} else {
				System.out.println("No Data to write into a file");
			}
		} catch (IOException e) {
			System.out.println("Error while writing to a file");
		}

		return writeCount;
	}

	// checks entry set of a hashMap with Value and return key
	public static <K, V> K getKey(Map<K, V> map, V value) {
		for (Map.Entry<K, V> entry : map.entrySet()) {
			if (value.toString().equalsIgnoreCase(entry.getValue().toString())) {
				return entry.getKey();
			}
		}
		return null;
	}

	private int spreadSheetCalculator(String[] inputs, Integer outputKeyIndex, Integer keyIndexA, Integer keyIndexB) {

		int resultCounts = 0;

		if (inputs[0].matches("-?\\d+") && inputs[2].matches("-?\\d+")) {
			resultCounts = 0;
			Double outputVal = 0.0;

			switch (inputs[1]) {
			case "+":
				outputVal = Double.valueOf(inputs[0]) + Double.valueOf(inputs[2]);
				break;
			case "-":
				outputVal = Double.valueOf(inputs[0]) - Double.valueOf(inputs[2]);
				break;
			case "*":
				outputVal = Double.valueOf(inputs[0]) * Double.valueOf(inputs[2]);
				break;
			case "/":
				outputVal = Double.valueOf(inputs[0]) / Double.valueOf(inputs[2]);
				break;
			}
			for (int i = 1; i < tableColumnData.size(); i++) {
				tableColumnData.get(i).put(outputKeyIndex, String.valueOf(Math.round(outputVal)));
				resultCounts++;

			}
		} else if ((inputs[0].matches("-?\\d+")) && (inputs[2].matches("^[a-zA-Z]+$"))) {
			resultCounts = 0;
			for (HashMap<Integer, String> val : tableColumnData) {
				try {
					keyIndexA = getKey(val, inputs[2]);
				} catch (NullPointerException e) {
					System.out.println("ColumnName not found :" + inputs[2]);
					return 0;

				}
				break;
			}
			for (int i = 1; i < tableColumnData.size(); i++) {
				Double output = 0.0;
				switch (inputs[1]) {
				case "+":
					output = Double.parseDouble(inputs[0]) + Double.parseDouble(tableColumnData.get(i).get(keyIndexA));
					break;
				case "-":
					output = Double.parseDouble(inputs[0]) - Double.parseDouble(tableColumnData.get(i).get(keyIndexA));
					break;
				case "*":
					output = Double.parseDouble(inputs[0]) * Double.parseDouble(tableColumnData.get(i).get(keyIndexA));
					break;
				case "/":
					output = Double.parseDouble(inputs[0]) / Double.parseDouble(tableColumnData.get(i).get(keyIndexA));
					break;
				}
				tableColumnData.get(i).put(outputKeyIndex, String.valueOf(Math.round(output)));
				resultCounts++;

			}
		} else if ((inputs[0].matches("^[a-zA-Z]+$")) && (inputs[2].matches("-?\\d+"))) {
			resultCounts = 0;
			for (HashMap<Integer, String> val : tableColumnData) {
				try {
					keyIndexA = getKey(val, inputs[0]);
				} catch (NullPointerException e) {
					System.out.println("ColumnName not found :" + inputs[0]);
					return 0;

				}
				break;
			}
			for (int i = 1; i < tableColumnData.size(); i++) {
				Double output = 0.0;
				switch (inputs[1]) {
				case "+":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA)) + Double.parseDouble(inputs[2]);
					break;
				case "-":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA)) - Double.parseDouble(inputs[2]);
					break;
				case "*":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA)) * Double.parseDouble(inputs[2]);
					break;
				case "/":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA)) / Double.parseDouble(inputs[2]);
					break;
				}

				tableColumnData.get(i).put(outputKeyIndex, String.valueOf(Math.round(output)));
				resultCounts++;
			}

		} else {
			resultCounts = 0;
			for (HashMap<Integer, String> val : tableColumnData) {
				try {
					keyIndexA = getKey(val, inputs[0]);
					keyIndexB = getKey(val, inputs[2]);
				} catch (NullPointerException e) {
					System.out.println("ColumnName not found ");
					return 0;

				}
				break;
			}
			for (int i = 1; i < tableColumnData.size(); i++) {
				Double output = 0.0;
				switch (inputs[1]) {
				case "+":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA))
							+ Double.parseDouble(tableColumnData.get(i).get(keyIndexB));
					break;
				case "-":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA))
							- Double.parseDouble(tableColumnData.get(i).get(keyIndexB));
					break;
				case "*":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA))
							* Double.parseDouble(tableColumnData.get(i).get(keyIndexB));
					break;
				case "/":
					output = Double.parseDouble(tableColumnData.get(i).get(keyIndexA))
							/ Double.parseDouble(tableColumnData.get(i).get(keyIndexB));
					break;
				}

				tableColumnData.get(i).put(outputKeyIndex, String.valueOf(Math.round(output)));
				resultCounts++;
			}

		}

		return resultCounts;
	}

}
